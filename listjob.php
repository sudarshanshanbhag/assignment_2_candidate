<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width , initial-scale=1, user-scalable=no" />
        <meta name="description" content="" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
       <link href="job_post_style.css" rel="stylesheet" />
    </head>
    <body>
    <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>

                </div>
            </div>
    </header>
        <div class="content-wrapper">
        <div class="container">
            <div class="col-lg-1"></div>

             <div class="col-lg-12"><br>
                  <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" ><strong>CANDIDATE LIST</strong></a></li>
                          
                 </ul>
                  <div class="tab-content">                          
                 <div class="table-responsve"><br>
                  <?php 
                     $link = mysqli_connect("localhost", "root", "", "cand_list");
 

                 if($link === false){
                    die("ERROR: Could not connect. " . mysqli_connect_error());
                          }
 

$sql = "SELECT * FROM candidates";
                     if($result = mysqli_query($link, $sql)){
    if(mysqli_num_rows($result) > 0){
                     echo "<table  class='table table-striped table-hover table-bordered' cellspacing='0' width='100%' >";
        echo "<thead>";
            echo "<tr>";
               echo " <th>First Name</th>";
                echo "<th>Last Name</th>";
                echo "<th>Dob</th>";
                echo "<th>Gender</th>";
                echo "<th>Mobile</th>";
                echo "<th>Landline</th>";
                echo "<th>Address</th>";
                echo "<th>City</th>";
                echo "<th>State</th>";
                echo "<th>Country</th>";
                echo "<th>Heighest Qualification</th>";
                echo "<th>Marks</th>";
               
           echo "</tr>";
        echo "</thead>";
 
        
 
        echo "<tbody>" ;
        while($row = mysqli_fetch_array($result)){
           echo "<tr>";
                echo "<td>" . $row['fname'] . "</td>";
                echo "<td>" . $row['lname'] . "</td>";
                echo "<td>" . $row['dob'] . "</td>";
                echo "<td>" . $row['gender'] . "</td>";
                echo "<td>" . $row['mnum'] . "</td>";
                echo "<td>" . $row['lnum'] . "</td>";
                echo "<td>" . $row['address'] . "</td>";
                echo "<td>" . $row['city'] . "</td>";
                echo "<td>" . $row['state'] . "</td>";
                echo "<td>" . $row['country'] . "</td>";
                echo "<td>" . $row['hqualif'] . "</td>";
                echo "<td>" . $row['marks'] . "</td>";
                echo "<td><a href='edit.php?id=".$row['cand_id']."'>Edit</a></td>";
          echo  "</tr>";
        }
       echo "</tbody>";
   echo  "</table>";
         mysqli_free_result($result);
    } else{
        echo "No records matching your query were found.";
    }
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
 
// Close connection
mysqli_close($link);
                         ?>
                              </div>
                      
                 </div>
            </div>
            
        </div>
        </div>
        </body>
</html>